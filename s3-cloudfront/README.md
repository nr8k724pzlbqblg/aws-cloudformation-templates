# S3 Static Website with CloudFront
This template will 

- create a s3 bucket for a static website
- create the appropriate bucket policy
- Associate an SSL cert to a CloudFront distro
- create a CloudFront distro
- create / update a DNS record in r53

# Docs

https://aws.amazon.com/premiumsupport/knowledge-center/cloudfront-serve-static-website/

https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-custom-domain-walkthrough.html

# See it in action
I am the `webmaster` for Milton High School's Marching Band and I archived their old website using this exact template.

```
[  4:37PM ]  [ jpabian@AGSL034:~ ]
$ host old.miltonband.org
old.miltonband.org is an alias for daukowsx29ljh.cloudfront.net.
daukowsx29ljh.cloudfront.net has address 18.64.155.49
daukowsx29ljh.cloudfront.net has address 18.64.155.118
daukowsx29ljh.cloudfront.net has address 18.64.155.44
daukowsx29ljh.cloudfront.net has address 18.64.155.91
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:4c00:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:ce00:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:4e00:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:ae00:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:e600:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:a400:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:3600:1:c7be:d340:93a1
daukowsx29ljh.cloudfront.net has IPv6 address 2600:9000:2343:c00:1:c7be:d340:93a1
[  4:37PM ]  [ jpabian@AGSL034:~ ]
```

You can view the site here: [https://old.miltonband.org/](https://old.miltonband.org/)

I am not responsible to the original mess. The website was original created using something called `Webhooks` and it's a piece of garbage that used some kind of weird markup but rendered with javascript.  

# Notes
This template assumes a couple of things that need to exist before it works...
- There is a CERT in Amazon Certificate Manager.  Easy to set up and just needs to validate domain ownership / control
- The DNS Zone is set up in Route 53.  If not, no big deal. We just need to remove that part from the template and update the DNS records manually.

# Bitbucket Pipeline to S3
We can build a pipeline to sync files to S3.

https://bitbucket.org/atlassian/aws-s3-deploy/src/master/

https://bitbucket.org/atlassian/aws-cloudfront-invalidate/src/master/


# Email
Since there's a concern with Email, we use GSuite and you can see the `MX` records point to Google:

```
[  4:41PM ]  [ jpabian@AGSL034:~ ]
$ dig mx miltonband.org

; <<>> DiG 9.16.1-Ubuntu <<>> mx miltonband.org
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20626
;; flags: qr rd ad; QUERY: 1, ANSWER: 5, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;miltonband.org.                        IN      MX

;; ANSWER SECTION:
miltonband.org.         0       IN      MX      30 aspmx3.googlemail.com.
miltonband.org.         0       IN      MX      10 aspmx.l.google.com.
miltonband.org.         0       IN      MX      20 alt2.aspmx.l.google.com.
miltonband.org.         0       IN      MX      30 aspmx2.googlemail.com.
miltonband.org.         0       IN      MX      20 alt1.aspmx.l.google.com.

;; Query time: 50 msec
;; SERVER: 172.31.128.1#53(172.31.128.1)
;; WHEN: Thu Mar 10 16:41:20 EST 2022
;; MSG SIZE  rcvd: 232

[  4:41PM ]  [ jpabian@AGSL034:~ ]
```

