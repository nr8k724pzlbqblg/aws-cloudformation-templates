# AWS CloudFormation Templates

This repo will hold all the templates we are using so they can be reused as needed.

# Usage

It is probably the easiest to just deploy them via the AWS CloudFormation console at this point.  In the future it might make sense to revisit and consider the use of pipelines, etc.

## AWS Console
Simply go and upload the template, provide the parameters, and go.  I prefer to just upload them from my hard drive as opposed to going the extra steps and upload them to S3.  

## AWS CLI
It's also quick and easy to deploy via the CLI `provided you have an Access and Secret key`.  Ironically, there is a template to do that, so it's the old cart before the horse paradox and all of that.  

So if your access is set up, you can either put your parameters in a `json` file or provide them as part of the command line arguments.

```
aws cloudformation create-stack --stack-name stackName \
--template-body file://filepath/template.yaml \
--parameters ParameterKey=AppName,ParameterValue=MyCoolApp \
ParameterKey=Env,ParameterValue=qa \
--capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND --region us-west-2
```

# Testing
We'll use `taskcat` for testing of templates.  This will be updated later with more info.

# Notes, tips, etc
Here's some things I've learned over the years:

- Sure, it can happen but rarely does.  It is unlikely to get the template correct right away.  Don't get discouraged
- Make sure you are `tagging`. That's really important so humans can see who did what, and who to ask to
- It's going to happen so be prepare for unintended consequences.  What I mean is that even by using `IaC` and CloudFormations, someone somewhere and some point is going to manually change something and cause drift.  At that point, things won't go as expected or it will be difficult to troubleshoot.  
- Try to update the template for modifications and changes
- DO NOT use or rely on `exports`. They WILL get you into trouble.  I'll tell you how based on my personal experience over a beer or two.
- Ask questions! 